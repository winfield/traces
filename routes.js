'use strict';

import React from 'react';
import { Router, Route, IndexRoute, Redirect } from 'react-router'

import App from './js/components/App.react';
import ArticlesSection from './js/components/ArticlesSection.react';
import ArticleSection from './js/components/ArticleSection.react';
import NewArticle from './js/components/NewArticle.react';
import EditArticle from './js/components/EditArticle.react';
import NotFound from './js/components/NotFound.react';
import About from './js/components/About.react';

const routes = (
  <Router>
    <Route path="/" component={App}>
      <IndexRoute component={ArticlesSection} />
      <Route path="about" component={About} />
      <Redirect from="articles" to="/" />
      <Route path="articles">
        <Route path="new" component={NewArticle} />
        <Route path=":slug/edit" component={EditArticle} />
        <Route path=":slug" component={ArticleSection} />
      </Route>
    </Route>
  </Router>
);

export default routes;
