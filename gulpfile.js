'use strict';

var gulp = require('gulp');
var jshint = require('gulp-jshint');
var uglify = require('gulp-uglify');
var size = require('gulp-size');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var browserify = require('browserify');

var path = {
  JS: './js/app.js',
  MINIFIED: 'bundle.min.js',
  DEST_BUILD: 'public/js'
};

gulp.task('build', function() {
  browserify(path.JS)
  .bundle()
  .pipe(source(path.MINIFIED))
  .pipe(buffer())
  .pipe(uglify())
  .pipe(size())
  .pipe(gulp.dest(path.DEST_BUILD));
});

gulp.task('lint', function() {
  gulp.src(['./js/*/*.js', './js/app.js', './*.js',
            './models/*.js', './controllers/*.js',
            './routes/*.js'])
  .pipe(jshint({ linter: require('jshint-jsx').JSXHINT }))
  .pipe(jshint.reporter('default'));
});
