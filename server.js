'use strict';

import React from 'react';
import ReactDOMServer from 'react-dom/server';
import { match, RouterContext } from 'react-router'

import ArticleStore from './js/stores/ArticleStore';

import routes from './routes';

function parseData(data) {
  const dataForParse = Array.isArray(data) ? data : [data];
  let parsed = {};
  dataForParse.forEach(function(d) {
    parsed[d.slug] = d;
  });

  console.log(parsed);
  return parsed;
}

function postData(routes, params, data) {
  let result;
  return Promise.all(routes
    .filter(function(route) { return route.handler.postData; })
    .map(function(route) {
      return route.handler.postData(params, data).then(function(d) {
        result = parseData(d);
      });
    })
  ).then(function() { return result; });
}

function fetchData(components, params) {
  let result;
  return Promise.all(components
    .filter(component => component != undefined)
    .filter(component => component.fetchData)
    .map(component => {
      return component.fetchData(params).then(function(d) {
        result = parseData(d);
      });
    })
  ).then(function() { return result; });
}

export default function() {
  return async function serverRender(ctx, next) {
    const path = ctx.path;
    const method = ctx.method;
    if (method !== 'GET') {
      // TODO
    }

    const res = new Promise(
      function(resolve, reject) {
        match({ routes, location: path }, (error, redirectLocation, renderProps) => {
          if (error) {
            console.log(error);
          } else if (redirectLocation) {
            resolve({
              status: 302,
              location: redirectLocation.pathname + redirectLocation.search
            })
          } else if (renderProps) {
            if (method === 'GET') {
              fetchData(renderProps.components, renderProps.params).then(function(data) {
                if (data && data.status === 404) {
                  resolve({ status: 404 });
                } else {
                  ArticleStore.init(data);
                  resolve({
                    status: 200,
                    initialData: escape(JSON.stringify(data)),
                    body: ReactDOMServer.renderToString(<RouterContext {...renderProps} />)
                  });
                }
              });
            }
          } else {
            resolve({ status: 404 })
          }
        });
      }
    );

    const result = await res;
    ctx.status = result.status;
    if (result.status === 302) {
      ctx.redirect(result.location);
    }
    else {
      await ctx.render('layout', {
        body: function() { return result.body; },
        initialData: result.initialData
      });
    }
    await next();
  }
};
