'use strict';

export const signup = function *() {
  yield this.render('users/sign_up');
};
