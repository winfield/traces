'use strict';

export const index = function() {
  return async function(ctx, next) {
    if (ctx.session.user_id) return ctx.body = 'hello world';
    ctx.status = 401;
  }
};
