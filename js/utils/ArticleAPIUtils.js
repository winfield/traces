'use strict';

import req from 'superagent-promise';

const BASE = '/api/articles/';

function fullUrl() {
  return 'http://127.0.0.1:8888' + BASE;
}

function ArticleAPIUtils(serverSideRender) {
  const url = serverSideRender ? fullUrl() : BASE;

  return {
    index: function() {
      return req('GET', url)
      .set('Accept', 'application/json')
      .end()
      .then(function(res) {
        return JSON.parse(res.text);
      });
    },

    show: function(slug) {
      return req('GET', url + slug)
      .set('Accept', 'application/json')
      .end()
      .then(function(res) {
        return JSON.parse(res.text);
      })
      .catch(function(err) {
        return { status: err.status };
      });
    },

    create: function(article) {
      return req('POST', url)
      .send(article)
      .set('Accept', 'application/json')
      .end()
      .then(function(res) {
        return JSON.parse(res.text);
      });
    },

    update: function(slug, article) {
      return req('PUT', url + slug)
      .send(article)
      .set('Accept', 'application/json')
      .end()
      .then(function(res) {
        return JSON.parse(res.text);
      });
    }
  };
};

export default ArticleAPIUtils;
