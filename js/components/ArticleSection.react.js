'use strict';

import React, { Component } from 'react';
import Article from './Article.react';
import ArticleStore from '../stores/ArticleStore';
import ArticleActions from '../actions/ArticleActions';
import ArticleAPIUtils from '../utils/ArticleAPIUtils';

function getArticleState(slug) {
  return {
    article: ArticleStore.show(slug)
  };
}

export default class ArticleSection extends Component {
  constructor(props) {
    super(props);
    this.state = getArticleState(this.props.params.slug);
    this._onArticleChange = this._onArticleChange.bind(this);
  }

  static fetchData(params) {
    console.log('server side rendering');
    return ArticleAPIUtils('serverSideRender').show(params.slug).then(function(data) {
      return data;
    });
  }

  componentDidMount() {
    ArticleStore.addChangeListener(this._onArticleChange);
    ArticleActions.show(this.props.params.slug);
  }

  componentWillUnmount() {
    ArticleStore.removeChangeListener(this._onArticleChange);
  }

  _onArticleChange() {
    const state = getArticleState(this.props.params.slug);
    this.setState(state);
  }

  render() {
    if (!this.state.article) { return null; }

    return (
      <div>
        <div>Single Article Section</div>
        <Article article={this.state.article}/>
      </div>
    );
  }
}

ArticleSection.contextTypes = {
  router: React.PropTypes.object.isRequired
}

