'use strict';

import React, { Component } from 'react';

function _articleLink(slug) {
  return "/articles/" + slug;
}

export default class Article extends Component {
  constructor(props) {
    super(props);
    this._clickTitle = this._clickTitle.bind(this);
  }

  _clickTitle(e) {
    e.preventDefault();
    this.context.router.push(_articleLink(this.props.article.slug));
  }

  render() {
    const title = this.props.article.title;
    const body = this.props.article.body;
    const slug = this.props.article.slug;

    return (
      <article>
        <h1 onClick={this._clickTitle}><a href={_articleLink(slug)}>{title}</a></h1>
        <p>{body}</p>
      </article>
    );
  }
};

Article.contextTypes = {
  router: React.PropTypes.object.isRequired
}
