'use strict';

import React, { Component } from 'react';
import Editor from './Editor.react';
import ArticleStore from '../stores/ArticleStore';
import ArticleActions from '../actions/ArticleActions';
import ArticleAPIUtils from '../utils/ArticleAPIUtils';

function getArticleState(slug) {
  return {
    article: ArticleStore.show(slug)
  };
}

export default class EditArticle extends Component {
  constructor(props) {
    super(props);
    this.state = getArticleState(this.props.params.slug);
  }

  static fetchData(params) {
    console.log('server side rendering');
    return ArticleAPIUtils('serverSideRender').show(params.slug).then(function(data) {
      return data;
    });
  }

  static postData(params, data) {
    console.log('server side rendering');
    return ArticleAPIUtils('serverSideRender').update(params.slug, data).then(function(result) {
      return result;
    });
  }

  componentDidMount() {
    ArticleStore.addChangeListener(this._onArticleChange);
    ArticleActions.show(this.props.params.slug);
  }

  componentWillUnmount() {
    ArticleStore.removeChangeListener(this._onArticleChange);
  }

  _onArticleChange() {
    const state = getArticleState(this.props.params.slug);
    this.setState(state);
  }

  handleSubmit(e) {
    e.preventDefault();
    const title = e.target.title.value.trim();
    const body = e.target.body.value.trim();
    const slug = _articleSlug(this.context);
    ArticleActions.update(slug, { title: title, body: body }, this.context.router);
  }

  render() {
    if (!this.state.article) { return null; }

    return (
      <div>
        <Editor formHandler={this.handleSubmit} article={this.state.article} />
      </div>
    );
  }
}

EditArticle.contextTypes = {
  router: React.PropTypes.object.isRequired
}
