'use strict';

import React, { Component } from 'react';
import { Link } from 'react-router'

export default class App extends Component {
  render() {
    return (
      <div>
        <header>
          <ul>
            <li><Link to="/">Home</Link></li>
            <li><Link to="/about">About</Link></li>
            <li><Link to="/articles">Articles</Link></li>
            <li><Link to="/articles/new">New Article</Link></li>
          </ul>
        </header>

        {this.props.children}
      </div>
    );
  }
}
