'use strict';

import React, { Component } from 'react';
import Article from './Article.react';
import ArticleStore from '../stores/ArticleStore';
import ArticleActions from '../actions/ArticleActions';
import ArticleAPIUtils from '../utils/ArticleAPIUtils';

function getArticleState() {
  return {
    articles: ArticleStore.index()
  };
}

export default class ArticlesSection extends Component {
  constructor(props) {
    super(props);
    this.state = getArticleState();
    this._onArticleChange = this._onArticleChange.bind(this);
  }

  static fetchData(params) {
    console.log('server side rendering');
    return ArticleAPIUtils('serverSideRender').index().then(function(data) {
      return data;
    });
  }

  componentDidMount() {
    ArticleStore.addChangeListener(this._onArticleChange);
    ArticleActions.index();
  }

  componentWillUnmount() {
    ArticleStore.removeChangeListener(this._onArticleChange);
  }

  render() {
    const allArticles = this.state.articles;
    let articles = [];
    for (const key in allArticles) {
      articles.push(<Article key={key} article={allArticles[key]} />);
    }

    return (
      <ul>
        {articles}
      </ul>
    );
  }

  _onArticleChange() {
    this.setState(getArticleState());
  }
}
