'use strict';

import React, { Component } from 'react';

export default class Editor extends Component {
  render() {
    const title = this.props.article ? this.props.article.title : "";
    const body = this.props.article ? this.props.article.body : "";

    return (
      <form method="POST" onSubmit={this.props.formHandler}>
        <div>
          <input type="text" name="title" defaultValue={title} />
        </div>
        <div>
          <textarea name="body" defaultValue={body} />
        </div>
        <input type="submit" />
      </form>
    );
  }
}

