'use strict';

import React, { Component } from 'react';
import Editor from './Editor.react';
import ArticleActions from '../actions/ArticleActions';
import ArticleAPIUtils from '../utils/ArticleAPIUtils';

export default class NewArticle extends Component {
  static postData(params, data) {
    console.log('server side rendering');
    return ArticleAPIUtils('serverSideRender').create(data).then(function(result) {
      return result;
    });
  }

  handleSubmit(e) {
    e.preventDefault();
    const title = e.target.title.value.trim();
    const body = e.target.body.value.trim();
    ArticleActions.create({ title: title, body: body }, this.context.router);
  }

  render() {
    return (
      <div>
        <Editor formHandler={this.handleSubmit} />
      </div>
    );
  }
}

NewArticle.contextTypes = {
  router: React.PropTypes.object.isRequired
}
