'use strict';

import keyMirror from 'keymirror';

export default keyMirror({
  ARTICLES_INDEX: null,
  ARTICLES_SHOW: null
});
