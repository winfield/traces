'use strict';

import AppDispatcher from '../dispatcher/AppDispatcher';
import ArticleConstants from '../constants/ArticleConstants';
import ArticleAPIUtils from '../utils/ArticleAPIUtils';

const ArticleActions = {
  index: function() {
    ArticleAPIUtils().index().then(function(data) {
      AppDispatcher.dispatch({
        actionType: ArticleConstants.ARTICLES_INDEX,
        data: data
      });
    });
  },

  show: function(slug) {
    ArticleAPIUtils().show(slug).then(function(data) {
      AppDispatcher.dispatch({
        actionType: ArticleConstants.ARTICLES_SHOW,
        data: data
      });
    });
  },

  create: function(article, router) {
    ArticleAPIUtils().create(article).then(function(data) {
      router.transitionTo("/articles/" + data.slug);
      AppDispatcher.dispatch({
        actionType: ArticleConstants.ARTICLES_SHOW,
        data: data
      });
    });
  },

  update: function(slug, article, router) {
    ArticleAPIUtils().update(slug, article).then(function(data) {
      router.transitionTo("/articles/" + data.slug);
      AppDispatcher.dispatch({
        actionType: ArticleConstants.ARTICLES_SHOW,
        data: data
      });
    });
  }
};

export default ArticleActions;
