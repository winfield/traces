'use strict';

import AppDispatcher from '../dispatcher/AppDispatcher';
import ArticleConstants from '../constants/ArticleConstants';
import EventEmitter from 'events';
import assign from 'object-assign';

const CHANGE_EVENT = 'change';

let _articles = {};

function _addArticle(article) {
  _articles[article.slug] = article;
}

const ArticleStore = assign({}, EventEmitter.prototype, {
  init: function(data) {
    _articles = data;
  },

  index: function() {
    return _articles;
  },

  show: function(slug) {
    return _articles[slug];
  },

  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },

  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback);
  },

  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  },
});

export default ArticleStore;

AppDispatcher.register(function(action) {
  switch(action.actionType) {
    case ArticleConstants.ARTICLES_INDEX:
      action.data.forEach(function(article) {
        _addArticle(article);
      });
      ArticleStore.emitChange();
      break;

    case ArticleConstants.ARTICLES_SHOW:
      _addArticle(action.data);
      ArticleStore.emitChange();
      break;
  }
});
