'use strict';

import React from 'react';
import { render } from 'react-dom';
import { Router, browserHistory } from 'react-router'
import ArticleStore from './stores/ArticleStore';
import routes from '../routes';

ArticleStore.init(JSON.parse(unescape(window.__INITIAL_DATA__)));
render(<Router history={browserHistory} routes={routes} />, document.body)
