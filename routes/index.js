'use strict';

import * as home from '../controllers/home';
import * as users from '../controllers/users';
import * as articles from '../controllers/articles';

import router from 'koa-router';

export default router()
  .get('/', home.index())
  .get('/articles', articles.index())
  .get('/articles/:slug', articles.show())
  .post('/articles', articles.create())
  .put('/articles/:slug', articles.update());
