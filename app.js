'use strict';

import koa from 'koa';
import convert from 'koa-convert';
import mount from 'koa-mount';
import session from 'koa-session';
import serve from 'koa-static';
import views from 'koa-views';
import bodyParser from 'koa-bodyparser';

import pageNotFound from './middlewares/pageNotFound';

import router from './routes/index';
import serverRender from './server';

const api = new koa();
api.use(router.routes()).use(router.allowedMethods());

const app = new koa();
app.use(bodyParser());
app.use(views(__dirname + '/views', {
  map: { html: 'swig' }
}));
app.use(pageNotFound());
app.keys = ['secret1', 'secret2', 'secret3'];
app.use(mount('/api', api));
app.use(serve('./public'));
app.use(convert(session(app)));
app.use(serverRender());

const port = process.argv[2] || 8888;
console.log("started on port: " + port);
app.listen(port);
