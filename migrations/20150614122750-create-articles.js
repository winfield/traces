'use strict';

module.exports = {
  up: function (m, DataTypes) {
    m.createTable('Articles', {
      id: {
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4
      },
      title: DataTypes.STRING,
      body: DataTypes.TEXT,
      slug: DataTypes.STRING,
      createdAt: DataTypes.DATE,
      updatedAt: DataTypes.DATE
    });
  },

  down: function (m) {
    m.dropTable('Articles');
  }
};
