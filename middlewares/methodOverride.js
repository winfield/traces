'use strict';

var parse = require('co-body');
var override = require('koa-override-method')

export default const pageNotFound = function *(next) {
  var body = yield parse.form(this);
  var method = override.call(this, body);
  this.request.method = method;

  yield next;
}
