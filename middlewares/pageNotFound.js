'use strict';

export default function() {
  return async function pageNotFound(ctx, next) {
    await next();

    if (404 != ctx.status) return;
    ctx.status = 404;
    await ctx.render('404.html');
  };
};
