"use strict";

var rewire = require('rewire');
var model = rewire('../../models/article');
var chai = require("chai");
var expect = chai.expect;
var factories = require('chai-factories');
var articleFactory = require('../factories/article');

chai.use(factories);
chai.factory('article', articleFactory);
var article = chai.create('article', { id: 'abc-123-4931342521e' });

describe('Article', function() {
  describe('#generateSlug()', function () {
    var generateSlug;

    before(function() {
      generateSlug = model.__get__('generateSlug');
    })

    it('generates correct slug from article info', function() {
      expect(generateSlug(article)).to.equal('a-song-of-ice-and-fire-4931342521e');
    });
  });
});
